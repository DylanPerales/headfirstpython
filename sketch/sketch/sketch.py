import nester
import pickle

man = []
other = []

try:
    data = open('sketch.txt')
    for each_line in data:
        try:
            (role, line_spoken) = each_line.split(':', 1)
            line_spoken = line_spoken.strip()
            if role == 'Man':
                man.append(line_spoken)
            elif role == 'Other Man':
                other.append(line_spoken)
        except ValueError:
            pass
    data.close()
except IOError:
    print('The data file is missing! (sketch.txt)')
    
try:
    with open('man_data.txt', 'wb') as man_file, open('other_data.txt', 'wb') as other_file:
        #Older print to file
    	#nester.print_lol (man, fh=man_file)
    	#nester.print_lol (other, fh=other_file)
        pickle.dump(man, man_file)
        pickle.dump(other, other_file)
    
except IOError as err:
    print('File error: ' + str(err))
except pickle.PickleError as perr:
    print('Pickling erro: ' + str(perr))
    
""" 'No Longer needed with 'with' open statement'
finally:
    man_file.close()
    other_file.close()"""

""" 'Read data to verify after'
with open('man_data.txt') as mdf:
    print(mdf.readline())"""
