"""nester.py module. Contains print_lol() function which prints lists that
may or may not include nested lists, optional arguments to indent, level of indentation
and optional file to write"""

import sys

def print_lol(the_list, indent=False, level=0, fh=sys.stdout):
        """This function takes a positional argument 'the_list', which is a python
        list (possibly nested lists). Each data item in the list is (recursively)
        printed. A second argument called "level" is used to insert tab stops when
        a nested list is encountered."""
        for each_item in the_list:
                if isinstance(each_item, list):
                        print_lol(each_item, indent, level+1, fh)
                else:
                        if indent:
                                for tab_stop in range(level):
                                        print ("\t", end='', file=fh)
                        else:
                                print(each_item, file = fh)
