from distutils.core import setup

setup(
	name			= 'nester',
	version 		= '1.3.0',
	py_modules		= ['nester'],
	author			= 'dylan',
	author_email	        = 'dperales@gmail.com',
	url			= 'http://www.peralysis.com',
	description		= 'A simple printer of nested lists',
	)
	
	
